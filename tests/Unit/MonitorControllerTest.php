<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Spatie\UptimeMonitor\Models\Monitor;
use Tests\TestCase;

class MonitorControllerTest extends TestCase
{
    protected User $user;

    public function setUp(): void
    {
        parent::setup();
        $this->user = $this->getUser();
        DB::beginTransaction();
    }

    /**
     * Test monitor creates
     */
    public function testCreate()
    {
        $url = $this->getSampleUrl();
        $response = $this->actingAs($this->user)
            ->post(route('monitors.create'), [
                'url' => $url
            ]);

        $response->assertStatus(201);
        $this->assertDatabaseHas('monitors', [
            'url' => $url
        ]);
    }

    public function testIndex()
    {
        $response = $this->actingAs($this->user)
            ->get(route('monitors.index'));

        $response->assertStatus(200);
    }

    /**
     * Test monitors check
     */
    public function testCheck()
    {
        $response = $this->actingAs($this->user)
            ->post(route('monitors.check'), [
                'uptime' => true,
                'certificate' => true
            ]);

        $response->assertStatus(204);
    }

    /**
     * Test monitor destroys
     */
    public function testDestroy()
    {
        $monitor = Monitor::create([
            'url' => 'test2.test.com'
        ]);
        $response = $this->actingAs($this->user)
            ->delete(route('monitors.destroy', [ 'monitor' => $monitor->id ]));

        $response->assertStatus(204);
        $this->assertFalse(Monitor::where('id', $monitor->id)->exists());
    }

    /**
     * Gets a user instance for testing purposes
     */
    private function getUser(): User
    {
        return User::first();
    }

    /**
     * Gets a sample url for testing purposes
     */
    private function getSampleUrl(): string
    {
        return 'https://test.com';
    }

    public function tearDown(): void
    {
        DB::rollBack();
        parent::tearDown();
    }
}
