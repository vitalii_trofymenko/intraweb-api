<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    /**
     * Test email and password requirements
     *
     * @return void
     */
    public function testRequiresEmailAndPassword()
    {
        $response = $this->json('POST', route('auth.login'));

        $response->assertStatus(422);
    }

    /**
     * Test login
     *
     * @return void
     */
    public function testLogin()
    {
        $response = $this->json('POST', route('auth.login'), [
            'email' => 'test@test.com',
            'password' => 123123
        ]);

        $response->assertStatus(204);
    }

    /**
     * Test get auth user data
     *
     * @return void
     */
    public function testGetUser()
    {
        $user = User::first();
        $response = $this->actingAs($user)
            ->get(route('auth.user'));

        $response->assertJson([
            'data' => [
                'name' => $user->name
            ]
        ])->assertStatus(200);
    }

    /**
     * Test logout
     *
     * @return void
     */
    public function testLogout()
    {
        $response = $this->json('POST', route('auth.logout'));

        $response->assertStatus(204);
    }
}
