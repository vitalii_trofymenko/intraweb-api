<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'admin',
            'email' => 'test@test.com',
            'password' => \Illuminate\Support\Facades\Hash::make('123123'),
        ]);
    }
}
