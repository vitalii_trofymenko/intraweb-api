<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'v1'], function () {
    Route::prefix('auth')->group(function () {
        Route::post('login', 'AuthController@login')->name('auth.login');
        Route::post('logout', 'AuthController@logout')->name('auth.logout');
    });
    Route::group(['middleware' => 'auth:airlock'], function () {
        Route::prefix('auth')->group(function () {
            Route::get('user', 'AuthController@getUser')->name('auth.user');
        });
        Route::prefix('monitors')->group(function () {
            Route::get('', 'MonitorController@index')->name('monitors.index');
            Route::post('', 'MonitorController@create')->name('monitors.create');
            Route::post('check', 'MonitorController@check')->name('monitors.check');
            Route::delete('{monitor}', 'MonitorController@destroy')->name('monitors.destroy');
        });
    });
});
