<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckMonitorsRequest;
use App\Http\Requests\CreateMonitorRequest;
use App\Http\Resources\MonitorResourceCollection;
use App\Services\MonitorService;
use Illuminate\Http\Request;
use Spatie\UptimeMonitor\Models\Monitor;

class MonitorController extends Controller
{
    protected MonitorService $monitorService;

    public function __construct(MonitorService $monitorService)
    {
        $this->monitorService = $monitorService;
    }

    public function index(Request $request)
    {
        $monitors = $this->monitorService->paginated($request->get('take'));

        return new MonitorResourceCollection($monitors);
    }

    public function create(CreateMonitorRequest $request)
    {
        $this->monitorService->create($request->all());

        return response(null, 201);
    }

    public function check(CheckMonitorsRequest $request)
    {
        if ($request->get('uptime')) {
            $this->monitorService->checkUptime();
        }
        if ($request->get('certificate')) {
            $this->monitorService->checkCertificate();
        }

        return response(null, 204);
    }

    public function destroy(Monitor $monitor)
    {
        try {
            $this->monitorService->destroy($monitor);
        } catch (\Exception $exception) {
            report($exception);
        }

        return response(null, 204);
    }
}
