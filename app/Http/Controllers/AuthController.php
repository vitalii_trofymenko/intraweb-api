<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Resources\AuthUserResource;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'message' => 'Credentials is invalid'
            ],401);
        }

        return response(null, 204);
    }

    public function getUser()
    {
        return response()->json([
            'data' => new AuthUserResource(auth()->user())
        ], 200);
    }

    public function logout()
    {
        Auth::logout();

        return response(null, 204);
    }
}
