<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MonitorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'url' => $this->raw_url,
            'uptime' => [
                'status' => $this->uptime_status,
                'lastCheck' => $this->uptime_last_check_date ? $this->uptime_last_check_date->diffForHumans() : null
            ],
            'certificate' => [
                'status' => $this->certificate_status,
                'expireAt' => $this->certificate_expiration_date ? $this->certificate_expiration_date->diffForHumans() : null
            ]
        ];
    }
}
