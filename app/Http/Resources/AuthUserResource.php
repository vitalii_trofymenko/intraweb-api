<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AuthUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'avatar' => [
                'url' => 'https://secure.gravatar.com/avatar/' . md5($this->email)
            ],
            'name' => $this->name
        ];
    }
}
