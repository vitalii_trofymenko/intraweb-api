<?php

namespace App\Services;

use Illuminate\Support\Facades\Artisan;
use Spatie\UptimeMonitor\Models\Enums\CertificateStatus;
use Spatie\UptimeMonitor\Models\Enums\UptimeStatus;
use Spatie\UptimeMonitor\Models\Monitor;
use Spatie\Url\Url;

class MonitorService
{
    public function create(array $attributes)
    {
        $url = Url::fromString($attributes['url']);

        Monitor::create([
            'url' => trim($url, '/'),
            'certificate_check_enabled' => $url->getScheme() === 'https',
            'uptime_status' => UptimeStatus::NOT_YET_CHECKED,
            'certificate_status' => CertificateStatus::NOT_YET_CHECKED
        ]);
    }

    public function paginated($take = 10)
    {
        return Monitor::latest()->paginate($take);
    }

    public function checkUptime()
    {
        return Artisan::call('monitor:check-uptime --force');
    }

    public function checkCertificate()
    {
        return Artisan::call('monitor:check-certificate');
    }

    public function destroy(Monitor $monitor)
    {
        return $monitor->delete();
    }
}
